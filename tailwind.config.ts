import colors from 'tailwindcss/colors';
import type {Config} from 'tailwindcss';
import plugin from 'tailwindcss/plugin';

const accentColor = colors.blue;

const customConfig = {
  mode: 'jit',
  content: [
    './src/**/*.{html,ts}',
  ],
  theme: {
    extend: {
      colors: {
        'background': '#eeeeee',
        'accent': {
          ...accentColor,
          DEFAULT: accentColor['500']
        },
      },
      fontFamily: {},
      spacing: {
        'footer': '15rem',
        'header': '3.5rem',
        '100vh': '100vh',
      },
      textShadow: {
        sm: '0 0 1px var(--tw-shadow-color)',
        DEFAULT: '0 0 1px var(--tw-shadow-color)',
      },
    },
  },
  plugins: [
    plugin(function ({matchUtilities, theme}) {
      matchUtilities(
        {
          'text-shadow': (value) => ({
            textShadow: value,
          }),
        },
        {values: theme('textShadow')}
      );
    }),
  ],
} satisfies Config;

export default customConfig;
