import {Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterLinkActive, RouterOutlet} from '@angular/router';
import {CardModule} from 'primeng/card';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, CardModule, RouterLinkActive],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
}
