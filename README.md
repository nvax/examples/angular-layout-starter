# Angular Layout Starter

Example Layout for an Angular App using [tailwindcss](https://tailwindcss.com/) and [PrimeNG](https://primeng.org/).

See [Live Preview](https://angular-layout-starter-nvax-examples-deb0bbd173ab581dd2fbb0b2d4.gitlab.io/).
